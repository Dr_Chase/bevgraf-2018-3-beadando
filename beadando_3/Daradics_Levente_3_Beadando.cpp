#include <GL/glut.h>
#include <bevgrafmath2017.h>
#include <cmath>
#include <vector>
#include <iostream>

class Bullet {
	public:
		vec2 position;
		vec2 directionAndSpeed;
		Bullet() {
			position.x = 0.0f;
			position.y = 0.0f;

			directionAndSpeed.x = 0.0f;
			directionAndSpeed.y = 0.0f;
		}

		Bullet(vec2 pos) {
			this->position = pos;
		}
};

GLsizei winWidth = 600;
GLsizei winHeight = 600;

GLsizei winWidthZero = 0;
GLsizei winHeightZero = 0;

// the UFO

vec2 origoOfUFOEllipse((float)(winWidth / 2) , (float)(winHeight / 3 * 2));
vec2 speedAndDirectionOfUFO(1.0, 0);
double widthOfUFOEllipse = 40.0;
double heightOfUFOEllipse = 12.5;
double radiusOfSemiCircle = 15.0;
vec2 origoOfSemiCircle(origoOfUFOEllipse.x, origoOfUFOEllipse.y + (float)heightOfUFOEllipse);

float maxSpeedOfUFO = 7.0f;
float speedIncrement = 0.5f;

unsigned char UFORed = 255;
unsigned char UFOGreen = 255;
unsigned char UFOBlue = 255;

// da bulletz
std::vector<Bullet> daBulletz(0);

int globalBulletIterator = 0;

// da Base
vec2 baseRectPoint1;
vec2 baseRectPoint2;
vec2 baseRectPoint3;
vec2 baseRectPoint4;

vec2 cannonBarrelLowerPoint( (float)(winWidth / 2), 80.0f);
vec2 cannonBarrelUpperPoint((float)(winWidth / 2), 100.0f);

float cannonBallSpeed = 3.0f;
float cannonBarrelLengthMultiplier = 30.0f;

// mouse
GLint xMousePosition;
GLint yMousePosition;

// game logic

int nNumOfTotalShots = 0;

int nHealthPowerOfUFO = 10;

bool gameLost = false;
bool gameWin = false;

vec2 makeDirectionVector(vec2 point1, vec2 point2) {
	return point2 - point1;
}

void initGameLogic() {
	nNumOfTotalShots = 0;

	nHealthPowerOfUFO = 10;

	gameLost = false;
	gameWin = false;
}

void initBulletzLeft(float leftStartingX = 20.0f, float leftStartingY = 100.0f) {

	float xOffset = 0.0f;
	float yOffset = 0.0f;

	float spaceBetweenBullets = 10.0f;

	int nMaxBulletsPerRow = 1;
	int nBulletsPerRow = 0;
	int nActualRow = 1;
	for (int index = 0; index < 28; index++) {
		if (nBulletsPerRow == nMaxBulletsPerRow) {
			nBulletsPerRow = 0;
			++nMaxBulletsPerRow;
			++nActualRow;
			yOffset = yOffset + spaceBetweenBullets;
			xOffset = 0.0f;
		}
		Bullet bullet(vec2(leftStartingX + xOffset, leftStartingY - yOffset));
		daBulletz.push_back(bullet);
		xOffset = xOffset + spaceBetweenBullets;
		++nBulletsPerRow;
	}
}

void initBulletzRight(float leftStartingX = 20.0f, float leftStartingY = 100.0f) {

	leftStartingX = (float)winWidth - leftStartingX;

	float xOffset = 0.0f;
	float yOffset = 0.0f;

	float spaceBetweenBullets = 10.0f;

	int nMaxBulletsPerRow = 1;
	int nBulletsPerRow = 0;
	int nActualRow = 1;
	for (int index = 0; index < 28; index++) {
		if (nBulletsPerRow == nMaxBulletsPerRow) {
			nBulletsPerRow = 0;
			++nMaxBulletsPerRow;
			++nActualRow;
			yOffset = yOffset + spaceBetweenBullets;
			xOffset = 0.0f;
		}
		Bullet bullet(vec2(leftStartingX - xOffset, leftStartingY - yOffset));
		daBulletz.push_back(bullet);
		xOffset = xOffset + spaceBetweenBullets;
		++nBulletsPerRow;
	}
}

void initializeBulletz(float leftStartingX = 20.0f, float leftStartingY = 100.0f) {
	globalBulletIterator = 0;
	daBulletz.erase(daBulletz.begin(), daBulletz.end());
	initBulletzLeft(leftStartingX, leftStartingY);
	initBulletzRight(leftStartingX, leftStartingY);
}

void initializeBaseRect(float size = 80.0f) {
	baseRectPoint1.x = ((float)winWidth / 2.0f) - size / 2.0f; baseRectPoint1.y = (float)winHeightZero;
	baseRectPoint2.x = ((float)winWidth / 2.0f) + size / 2.0f; baseRectPoint2.y = (float)winHeightZero;

	baseRectPoint3.x = ((float)winWidth / 2.0f) + size / 2.0f; baseRectPoint3.y = (float)winHeightZero + size;
	baseRectPoint4.x = ((float)winWidth / 2.0f) - size / 2.0f; baseRectPoint4.y = (float)winHeightZero + size;

	printMathObject(baseRectPoint1);
	printMathObject(baseRectPoint2);
	printMathObject(baseRectPoint3);
	printMathObject(baseRectPoint4);
}

void initializeBase(float size = 80.0f) {
	initializeBaseRect(size);

	cannonBarrelLowerPoint = vec2((float)(winWidth / 2), 80.0f);
	cannonBarrelUpperPoint = vec2((float)(winWidth / 2), 100.0f);

	cannonBallSpeed = 3.0f;
	cannonBarrelLengthMultiplier = 30.0f;

}

void initUFO() {
	origoOfUFOEllipse = vec2((float) (winWidth / 2), (float)(winHeight / 3 * 2));
	speedAndDirectionOfUFO = vec2(1.0, 0);
	widthOfUFOEllipse = 40.0;
	heightOfUFOEllipse = 12.5;
	radiusOfSemiCircle = 15.0;
	origoOfSemiCircle= vec2(origoOfUFOEllipse.x, origoOfUFOEllipse.y + (float)heightOfUFOEllipse);

	maxSpeedOfUFO = 7.0f;
	speedIncrement = 0.5f;

	UFORed = 255;
	UFOGreen = 255;
	UFOBlue = 255;
}

void initAll() {
	glClearColor(1.0, 1.0, 1.0, 0.0);

	initUFO();
	initGameLogic();
	initializeBulletz(10.0f, 75.0f);
	initializeBase();
}

void init()
{
	glClearColor(1.0, 1.0, 1.0, 0.0);
	glMatrixMode(GL_PROJECTION);
	gluOrtho2D(winWidthZero, winWidth, winHeightZero, winHeight);
	glShadeModel(GL_FLAT);
	glEnable(GL_POINT_SMOOTH);

	initAll();
}

void drawSemiCircle(vec2 origoOfSemiCircle, GLdouble radiusOfSemiCircle)
{
	glPointSize(3.5);
	glColor3ub(0, 0, 0);
	glBegin(GL_POINTS);
	for (GLdouble t = 0; t <= pi(); t += 0.01)
		glVertex2d(origoOfSemiCircle.x + radiusOfSemiCircle * cos(t), origoOfSemiCircle.y + radiusOfSemiCircle * sin(t));
	glEnd();

	glColor3ub(UFORed, UFOGreen, UFOBlue);
	glBegin(GL_POLYGON);
	for (GLdouble t = 0; t <= pi(); t += 0.01)
		glVertex2d(origoOfSemiCircle.x + radiusOfSemiCircle * cos(t), origoOfSemiCircle.y + radiusOfSemiCircle * sin(t));
	glEnd();
}

void drawEllipse(vec2 origoOfEllipse, GLdouble widthOfEllipse, GLdouble heightOfEllipse)
{
	glPointSize(3.5);
	glColor3ub(0, 0, 0);
	glBegin(GL_POINTS);
	for (GLdouble t = 0; t <= 2 * pi(); t += 0.01)
		glVertex2d(origoOfEllipse.x + widthOfEllipse * cos(t), origoOfEllipse.y + heightOfEllipse * sin(t));
	glEnd();

	glColor3ub(UFORed, UFOGreen, UFOBlue);
	glBegin(GL_POLYGON);
	for (GLdouble t = 0; t <= 2 * pi(); t += 0.01)
		glVertex2d(origoOfEllipse.x + widthOfEllipse * cos(t), origoOfEllipse.y + heightOfEllipse * sin(t));
	glEnd();
}

void drawBulletz() {
	glPointSize(10.0);
	glColor3ub(0, 0, 0);
	glBegin(GL_POINTS);
	for (Bullet bullet : daBulletz) {
		glVertex2f(bullet.position.x, bullet.position.y);
	}
	glEnd();
}

void drawBaseRect() {
	glColor3ub(0, 240, 0);
	glBegin(GL_POLYGON);
		glVertex2f(baseRectPoint1.x, baseRectPoint1.y);
		glVertex2f(baseRectPoint2.x, baseRectPoint2.y);
		glVertex2f(baseRectPoint3.x, baseRectPoint3.y);
		glVertex2f(baseRectPoint4.x, baseRectPoint4.y);
	glEnd();
	
} 

void drawCannon() {

	glColor3ub(0, 0, 0);
	glLineWidth(5.0);
	glBegin(GL_LINES);
		glVertex2f(cannonBarrelLowerPoint.x, cannonBarrelLowerPoint.y);
		glVertex2f(cannonBarrelUpperPoint.x, cannonBarrelUpperPoint.y);
	glEnd();
}

void drawBase() {
	drawBaseRect();
	drawCannon();
}

void drawUFO() {
	drawEllipse(origoOfUFOEllipse, widthOfUFOEllipse, heightOfUFOEllipse);
	drawSemiCircle(origoOfSemiCircle, radiusOfSemiCircle);
}

void drawEndOfGame() {
	if (gameLost) {
		glClearColor(1.0, 0.0, 0.0, 0.0);
	}
	if (gameWin) {
		glClearColor(0.0, 1.0, 0.0, 0.0);
	}
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(0.0, 0.0, 1.0);

	drawUFO();

	drawBulletz();

	drawBase();

	drawEndOfGame();

	glutSwapBuffers();
}

void updateUFOMovement() {
	if ((origoOfUFOEllipse.x - (widthOfUFOEllipse)) <= (double)winWidthZero) {
		speedAndDirectionOfUFO.x = std::abs(speedAndDirectionOfUFO.x);
		if (maxSpeedOfUFO >= std::abs(speedAndDirectionOfUFO.x)) {
			speedAndDirectionOfUFO.x = speedAndDirectionOfUFO.x + speedIncrement;
		}
	}

	if((origoOfUFOEllipse.x + (widthOfUFOEllipse)) >= (double)winWidth) {
		speedAndDirectionOfUFO.x = std::abs(speedAndDirectionOfUFO.x) * -1.0f;
		if (maxSpeedOfUFO >= std::abs(speedAndDirectionOfUFO.x)) {
			speedAndDirectionOfUFO.x = speedAndDirectionOfUFO.x - speedIncrement;
		}
	}
	origoOfUFOEllipse = origoOfUFOEllipse + speedAndDirectionOfUFO;
	origoOfSemiCircle = vec2(origoOfUFOEllipse.x, origoOfUFOEllipse.y + (float)heightOfUFOEllipse) ;
}

vec2 makeNormalizedVectorFromBarrelDirection() {
	vec2 temporary;
	temporary.x = (float)xMousePosition;
	temporary.y = (float)yMousePosition;
	vec2 normalizedCannonVec = normalize(makeDirectionVector(cannonBarrelLowerPoint, temporary));
	return normalizedCannonVec;
}

void updateCannonCoordinates() {

	//cannonBarrelUpperPoint.x = (float)xMousePosition;
	//cannonBarrelUpperPoint.y = (float)yMousePosition;

	vec2 normalizedCannonVec = makeNormalizedVectorFromBarrelDirection();

	cannonBarrelUpperPoint = cannonBarrelLowerPoint + normalizedCannonVec * cannonBarrelLengthMultiplier;
}

// radiusOfSemiCircle
// origoOfSemiCircle
bool isPointInsideTheCircle(vec2 thePoint, vec2 theCirclesOrigo, double theRayOfCircle) {
	return (pow((thePoint.x - theCirclesOrigo.x), 2.0) + pow((thePoint.y - theCirclesOrigo.y), 2.0)) - pow(theRayOfCircle, 2.0) < 0.0;
}

bool isBulletHitTheUFO(vec2 bulletPoint) {
	// is bullet inside ellipse?

	float temp1 = (float)
	((std::pow((origoOfUFOEllipse.x - bulletPoint.x), 2.0) / std::pow(widthOfUFOEllipse, 2.0)) +
	(std::pow((origoOfUFOEllipse.y - bulletPoint.y), 2.0) / std::pow(heightOfUFOEllipse, 2.0))
	- 1.0f);

	bool whetherCircleWasHit = isPointInsideTheCircle(bulletPoint, origoOfSemiCircle, radiusOfSemiCircle);
	
	bool output = false;
	if (temp1 <= 0.0f || whetherCircleWasHit) {
		output = true;
	}

	return output;
}



void updateCannonBallCoordinates() {
	for (int index = 0; index < (int)daBulletz.size(); index++){
		Bullet myBall = daBulletz.at(index);
		myBall.position += myBall.directionAndSpeed;
		daBulletz.at(index) = myBall;

		if (isBulletHitTheUFO(myBall.position)) {
			daBulletz.erase(daBulletz.begin() + index);
			globalBulletIterator--;
			nHealthPowerOfUFO--;
			//UFORed -= (unsigned char)24;
			UFOGreen -= (unsigned char)24;
			UFOBlue -= (unsigned char)24;
			std::cout << "Direct hit!\n";
		}

		if (myBall.position.x > (float)winWidth || myBall.position.x < (float)winWidthZero ||
			myBall.position.y >(float)winHeight || myBall.position.y < (float)winHeightZero) {
			daBulletz.erase(daBulletz.begin() + index);
			globalBulletIterator--;
		}
	}
}

void shootWithCannon() {
	if (globalBulletIterator < (int) daBulletz.size() && !(gameLost || gameWin)) {
		daBulletz.at(globalBulletIterator).position = cannonBarrelUpperPoint;

		vec2 newspeedAndDirection = daBulletz.at(globalBulletIterator).directionAndSpeed;
		newspeedAndDirection = makeNormalizedVectorFromBarrelDirection() * cannonBallSpeed;
		daBulletz.at(globalBulletIterator).directionAndSpeed = newspeedAndDirection;
		globalBulletIterator++;

		nNumOfTotalShots++;
		std::cout << "we shot !\n";
	}
	
}

void stopEveryMovement() {
	vec2 nullvec2(0.0, 0.0);
	for (Bullet &myBall : daBulletz) {
		myBall.directionAndSpeed = nullvec2;
		myBall.directionAndSpeed = nullvec2;
	}

	speedAndDirectionOfUFO = 0.0;
}

void updateGameLogic() {
	// win
	if (daBulletz.size() > 0 && nHealthPowerOfUFO <= 0) {
		stopEveryMovement();

		gameWin = true;
	}

	if (daBulletz.size() == 0 && nHealthPowerOfUFO >= 0) {
		stopEveryMovement();
		gameLost = true;
	}
}

void update(int n)
{

	updateUFOMovement();

	updateCannonBallCoordinates();

	updateCannonCoordinates();

	updateGameLogic();

	glutPostRedisplay();
	glutTimerFunc(5, update, 0);

}

void keyboard(unsigned char key, int x, int y)
{
	switch (key) {
	case 27:
		exit(0);
		break;
	case 13:
		initAll();
	}
}

void processMouse(GLint button, GLint action, GLint xMouse, GLint yMouse)
{
	if (button == GLUT_LEFT_BUTTON && action == GLUT_DOWN)
		shootWithCannon();
}

void processMouseActiveMotion(GLint xMouse, GLint yMouse)
{
	xMousePosition = xMouse;
	yMousePosition = winHeight - yMouse;
	glutPostRedisplay();

}

void processMousePassiveMotion(GLint xMouse, GLint yMouse)
{
	xMousePosition = xMouse;
	yMousePosition = winHeight - yMouse;
	glutPostRedisplay();
	
}

int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(winWidth, winHeight);
	glutInitWindowPosition(100, 100);
	glutCreateWindow(argv[0]);
	init();
	glutDisplayFunc(display);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(processMouse);
	glutMotionFunc(processMouseActiveMotion);
	glutPassiveMotionFunc(processMousePassiveMotion);
	glutTimerFunc(5, update, 0);
	glutMainLoop();
	return 0;
}
